#include <iostream>

#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"

#include "TFileHandler.h"
#include "FileReader.h"

// ./econt_processor.exe relayNumber runNumber econtNumber BX

// print all (64-bit) words in event
void event_dump(const Hgcal10gLinkReceiver::RecordRunning *rEvent){
  const uint64_t *p64(((const uint64_t*)rEvent)+1);
  for(unsigned i(0);i<rEvent->payloadLength();i++){
    std::cout << "Word " << std::setw(3) << i << " ";
    std::cout << std::hex << std::setfill('0');
    std::cout << "0x" << std::setw(16) << p64[i] << std::endl;
    std::cout << std::dec << std::setfill(' ');
  }
  std::cout << std::endl;
}

// print all (32-bit) words from first or second column in the event
void event_dump_32(const Hgcal10gLinkReceiver::RecordRunning *rEvent, bool second){
  const uint64_t *p64(((const uint64_t*)rEvent)+1);
  for(unsigned i(0);i<rEvent->payloadLength();i++){
    std::cout << "Word " << std::setw(3) << i << " ";
    std::cout << std::hex << std::setfill('0');
    const uint32_t word = second ? p64[i] : p64[i] >> 32;
    std::cout << "0x" << std::setw(8) << word << std::endl;
    std::cout << std::dec << std::setfill(' ');
  }
  std::cout << std::endl;
}

bool is_cafe_word(const uint32_t word) {
  return (word >> 8) == 16698110;
}

// returns location in this event of the n'th 0xfecafe... line
int find_cafe_word(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  if (cafe_word_loc > 0) {
    if (is_cafe_word(p64[cafe_word_loc])) {
      return cafe_word_loc;
    }
  } 
  else {
    std::cout << "oops" << std::endl;
  }
 
  int cafe_counter = 0;
  int cafe_word_idx = -1;
  for(unsigned i(0);i<rEvent->payloadLength();i++){
    const uint32_t word = p64[i];
    if (is_cafe_word(word)) { // if word == 0xfeca
      cafe_counter++;
      if (cafe_counter == n){
        cafe_word_idx = i;
        break;
      }
    }
  }

  if (cafe_word_idx == -1) {
    std::cout << "Could not find cafe word" << std::endl;
    exit(0);
  }
  else {
    return cafe_word_idx;
  }
}

// find the packet_no'th packet (4 words) after finding 0xfeca in a word (which denotes the start of the set of packets)
// only useful for accessing the first set of packets (between the first two 0xfeca)
void set_packet(uint32_t packet[4], const Hgcal10gLinkReceiver::RecordRunning *rEvent, bool second, int packet_no, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int start_word_idx = find_cafe_word(rEvent, 1, cafe_word_loc);

  for (unsigned j=0; j<4; j++) {
    int word_idx = start_word_idx + 1 + packet_no*4 + j;
    const uint32_t pack_word = second ? p64[word_idx] : p64[word_idx] >> 32;
    packet[j] = pack_word;
  }
}

// scintillator triggers during BX, find the time within bunch crosssing
// possible output is 0->31 (inclusive)
int get_scintillator_trigger_loc(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int timing_word_start_idx = find_cafe_word(rEvent, 3, cafe_word_loc) + 6;

  int trigger_loc = 0;
  for(unsigned i(timing_word_start_idx);i<rEvent->payloadLength();i=i+5){
    const uint32_t timing_word = p64[i];

    if (timing_word != 0) {
      // use built in function to find number of leading zeros
      trigger_loc = trigger_loc + __builtin_clz(timing_word); 
      break;
    }
    trigger_loc = trigger_loc + 32;
  }   

  // if could not find trigger location -> exit
  if (trigger_loc == -1) {
    exit(1);
  }

  return trigger_loc;
}

uint32_t pick_bits32(uint32_t number, int start_bit, int number_of_bits) {
  // Create a mask to extract the desired bits.
  uint32_t mask = (1 << number_of_bits) - 1;
  // Shift the number to the start bit position.
  number = number >> (32 - start_bit - number_of_bits);
  // Mask the number to extract the desired bits.
  uint32_t picked_bits = number & mask;

  return picked_bits;
}

uint64_t pick_bits64(uint64_t number, int start_bit, int number_of_bits) {
  // Create a mask to extract the desired bits.
  uint64_t mask = (1 << number_of_bits) - 1;
  // Shift the number to the start bit position.
  number = number >> (64 - start_bit - number_of_bits);
  // Mask the number to extract the desired bits.
  uint64_t picked_bits = number & mask;

  return picked_bits;
}

// packet counter is the first 4 bits
uint32_t get_packet_counter(uint32_t* packet) {
  return pick_bits32(packet[0], 0, 4);
}

// 12 locations, 2 bits long, immediately after the packet counter
void set_packet_locations(uint32_t packet_locations[12], uint32_t* packet) {
  for (int i=0; i<12; i++) {
    packet_locations[i] = pick_bits32(packet[0], 4+i*2, 2);
  }
}

uint64_t decode_tc_val(uint64_t value)
{
  uint64_t mant = value & 0x7;
  uint64_t pos  = (value >> 3) & 0xf;

  if(pos==0) return mant << 1 ;

  pos += 2;

  uint64_t decompsum = 1 << pos;
  decompsum |= mant << (pos-3);
  return decompsum << 1 ;
}


// 12 energies, 7 bits long, immediately after the packet energies
void set_packet_energies(uint64_t packet_energies[12], uint32_t* packet) {
  uint64_t packet64[4];
  for (int i=0; i<4; i++) {
    packet64[i] = packet[i];
  }

  // need two 64 bit words since all of the energies are 12*7 = 84 bits long
  // word 1 starts with the beginning of the energies
  uint64_t word1 = (packet64[0] << 28+32) + (packet64[1] << 28) + (packet64[2] >> 4);
  // word 2 are the last 64 bits of the packet (which is 128 bits long)
  uint64_t word2 = (packet64[2] << 32) + packet64[3];

  for (int i=0; i<12; i++) {
    if (i < 9) {
      // first 9 (0->8) energies fit in first word
      packet_energies[i] = pick_bits64(word1, i*7, 7);
    } 
    else {
      // 9th energy starts 27 bits into the second word
      packet_energies[i] = pick_bits64(word2, 27+(9-i)*7, 7);
    } 
  }
}

int main(int argc, char** argv){
  
  if(argc < 3){
    std::cerr << argv[0] << ": no relay and/or run numbers specified" << std::endl;
    return 1;
  }

  //Command line arg assignment
  //Assign relay and run numbers
  unsigned relayNumber(0);
  unsigned runNumber(0);
  unsigned linkNumber(0);
  unsigned econtNumber(0);
  unsigned BXNumber(0);

  // ./econt_processor.exe relayNumber runNumber econtNumber BX
  std::istringstream issRelay(argv[1]);
  issRelay >> relayNumber;
  std::istringstream issRun(argv[2]);
  issRun >> runNumber;
  std::istringstream issEcont(argv[3]);
  issEcont >> econtNumber;
  std::istringstream issBX(argv[4]);
  issBX >> BXNumber;

  //Create the file reader
  Hgcal10gLinkReceiver::FileReader _fileReader;

  //Make the buffer space for the records
  Hgcal10gLinkReceiver::RecordT<4095> *r(new Hgcal10gLinkReceiver::RecordT<4095>);

  //Set up specific records to interpet the formats
  const Hgcal10gLinkReceiver::RecordStarting *rStart((Hgcal10gLinkReceiver::RecordStarting*)r);
  const Hgcal10gLinkReceiver::RecordStopping *rStop ((Hgcal10gLinkReceiver::RecordStopping*)r);
  const Hgcal10gLinkReceiver::RecordRunning  *rEvent((Hgcal10gLinkReceiver::RecordRunning*) r);
  _fileReader.setDirectory(std::string("dat/Relay")+argv[1]);
  _fileReader.openRun(runNumber,linkNumber);

  uint64_t nEvents = 0;

  uint32_t packet[4];
  uint32_t packet_counter;
  uint32_t packet_locations[12];
  uint64_t packet_energies[12];

  // make histograms
  double energy_bin_edges[129];
  for (int i=0; i<128; i++) {
    energy_bin_edges[i] = decode_tc_val(i);
  }
  energy_bin_edges[128] = energy_bin_edges[127] + (energy_bin_edges[127]-energy_bin_edges[126]);

  TH1I** hsum = new TH1I*[12]; // energies of each super triger cell (STC)
  TH1I** hsumc = new TH1I*[12]; // energies of each super triger cell (STC)
  TH2I** hsum_trig = new TH2I*[12]; // 2D histogram of energies of STC vs scintillator trigger location
  TH2I** hsumc_trig = new TH2I*[12]; // 2D histogram of energies of STC vs scintillator trigger location
  
  TProfile** hsum_trig_prof = new TProfile*[12];
  TProfile** hsumc_trig_prof = new TProfile*[12];
  
  TH1I** hloc = new TH1I*[12]; // locations of most energetic TC in STC
  std::string hname;
  for (int i=0;i<12;i++) {
    hname = "energy_" + std::to_string(i);
    hsum[i] = new TH1I(hname.c_str(),hname.c_str(),128,0,128);
    hname = "energyc_" + std::to_string(i);
    hsumc[i] = new TH1I(hname.c_str(),hname.c_str(),128,energy_bin_edges);

    hname = "energytrig_" + std::to_string(i);
    hsum_trig[i] = new TH2I(hname.c_str(),hname.c_str(),128,0,128,288,0,288);
    hname = "energyctrig_" + std::to_string(i);
    hsumc_trig[i] = new TH2I(hname.c_str(),hname.c_str(),128,energy_bin_edges,288,0,288);

    hname = "energytrigprof_" + std::to_string(i);
    hsum_trig_prof[i] = new TProfile(hname.c_str(),hname.c_str(),288,0,288);
    hname = "energyctrigprof_" + std::to_string(i);
    hsumc_trig_prof[i] = new TProfile(hname.c_str(),hname.c_str(),288,0,288);

    std::string locname = "location_" + std::to_string(i);
    hloc[i] = new TH1I(locname.c_str(),locname.c_str(),4,0,4);
  }
  TH1I* h_trigger_loc = new TH1I("h_trigger_loc", "h_trigger_loc", 288, 0, 288);

  // to cache where the cafe separators are
  int scintillator_cafe_word_loc;
  int econt_cafe_word_loc;

  //Use the fileReader to read the records
  while(_fileReader.read(r)) {
    //Check the state of the record and print the record accordingly
    if(     r->state()==Hgcal10gLinkReceiver::FsmState::Starting){
      rStart->print();
      std::cout << std::endl;
    }

    else if(r->state()==Hgcal10gLinkReceiver::FsmState::Stopping){
      rStop->print();
      std::cout << std::endl;
    }
    //Else we have an event record 
    else{
      //Increment event counter and reset error state
      nEvents++;
     
      // if (nEvents < 2) {
      //   event_dump(rEvent);
      //   //event_dump_32(rEvent, false);
      //   //event_dump_32(rEvent, true);
      // }

      if (nEvents == 1) {
        scintillator_cafe_word_loc = find_cafe_word(rEvent, 3);
        econt_cafe_word_loc = find_cafe_word(rEvent, 1);
      }

      int trigger_loc = get_scintillator_trigger_loc(rEvent, scintillator_cafe_word_loc);
      //std::cout << trigger_loc << std::endl;
      h_trigger_loc->Fill(trigger_loc);

      int packet_no = BXNumber;

      bool second = !econtNumber; // econt0 -> second column, econt1 -> first column
      set_packet(packet, rEvent, second, packet_no, econt_cafe_word_loc);
      packet_counter = get_packet_counter(packet);
      set_packet_locations(packet_locations, packet);
      set_packet_energies(packet_energies, packet);

      for (int i=0;i<12;i++) {
        hsum[i]->Fill(packet_energies[i]);
        hsumc[i]->Fill(decode_tc_val(packet_energies[i]));
        hloc[i]->Fill(packet_locations[i]);
        hsum_trig[i]->Fill(packet_energies[i], trigger_loc);
        hsumc_trig[i]->Fill(decode_tc_val(packet_energies[i]), trigger_loc);

        hsum_trig_prof[i]->Fill(trigger_loc, packet_energies[i]);
        hsumc_trig_prof[i]->Fill(trigger_loc, decode_tc_val(packet_energies[i]));
      }
    }  
  }
  delete r;

  if (nEvents > 0) {
    // make directory for ROOT file
    std::string dir = "output/Relay" + std::to_string(relayNumber) + "/econt" + std::to_string(econtNumber) + "/";
    std::string command = "mkdir -p " + dir;
    system(command.c_str());

    // open ROOT file
    std::string filename = dir + "Run" + std::to_string(runNumber) + ".root";
    std::cout << filename << std::endl;
    TFile *file = TFile::Open(filename.c_str(),"UPDATE");
    //TFile *file = TFile::Open(filename.c_str(),"RECREATE");

    // create ROOT directory for BX
    std::string BX_dir_name = "BX" + std::to_string(BXNumber);
    TDirectory *BX = file->mkdir(BX_dir_name.c_str());
    BX->cd();
    
    // write histograms
    for (int i=0;i<12;i++) {
      hsum[i]->Write();
      hsumc[i]->Write();
      hsum_trig[i]->Write();
      hsumc_trig[i]->Write();
      hloc[i]->Write();

      hsum_trig_prof[i]->Write();
      hsumc_trig_prof[i]->Write();
    }
    h_trigger_loc->Write();
    delete file;
  }
  else {
    std::cout << "File had no events!" << std::endl;
    return 1;
  }

  return 0;
}
