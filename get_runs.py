#!/usr/bin/python3

import sys
import os

relay_dir = sys.argv[1]
all_files = os.listdir(relay_dir)

runs = []
for f in all_files:
  if "Run" not in f: continue
  runs.append(f.split("Run")[1].split("_")[0])

runs = sorted(list(set(runs)))
if len(sys.argv) > 2:
  length = int(sys.argv[2])
  runs = runs[:length]

print(" ".join(runs))
