import sys
import uproot
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import os

if __name__=="__main__":
  f = sys.argv[1]
  runNumber = f.split("Run")[1].split(".root")[0]
  relayNumber = f.split("Relay")[1].split("/")[0]

  outdir = "/".join(f.split("/")[:-1]).replace("output", "plots") + f"/Run{runNumber}/"
  os.makedirs(outdir, exist_ok=True)
  print(outdir)

  f = uproot.open(f)
  
  BXs = np.arange(9)
  STCs = np.arange(12)

  for STC in STCs:
    x = [BX for BX in BXs for j in range(128)]
    y = [j for BX in BXs for j in range(128)]
    
    values = np.array([f[f"BX{BX}/energy_{STC}"].values() for BX in BXs]).flatten()
    
    plt.hist2d(x, y, weights=values, bins=[9, 128], norm="log")
    plt.xlabel("BX")
    plt.ylabel("Energy")
    cbar = plt.colorbar()
    cbar.set_label("Number of events")
    plt.title(f"Relay {relayNumber}    Run {runNumber} ")
    plt.savefig(outdir+f"/energy_{STC}.png")
    plt.savefig(outdir+f"/energy_{STC}.pdf")
    plt.clf()

  fig, axs = plt.subplots(3, 4, sharex=True, sharey=True)#, layout="constrained")
  axs = axs.flatten()
  fig.subplots_adjust(hspace=0.0, wspace=0.0)
  for STC in STCs:
    x = [BX for BX in BXs for j in range(128)]
    y = [j for BX in BXs for j in range(128)]
    
    values = np.array([f[f"BX{BX}/energy_{STC}"].values() for BX in BXs]).flatten()
    

    ax = axs[STC]

    ax.text(0.5, 5, f"STC {STC}", verticalalignment="bottom", horizontalalignment="left")
    h, xedges, yedges, im = ax.hist2d(x, y, weights=values, bins=[9, 128], norm="log")
    
    if STC == 11:
      ax.set_xlabel("BX")
    elif STC == 0:
      ax.set_title(f"Relay {relayNumber}    Run {runNumber} ", loc="left")
      ax.set_ylabel("Energy")

  cbar = fig.colorbar(im, ax=axs)
  cbar.set_label("Number of events")
  plt.savefig(outdir+f"/energy_all.png")
  plt.savefig(outdir+f"/energy_all.pdf")
  f.close()




      