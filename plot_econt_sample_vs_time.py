import matplotlib
import matplotlib.pyplot as plt
import sys
import uproot
import numpy as np
import os
import tools

def plot(f, STC, ax, decoded=True):
  if decoded:
    hists = [f[f"BX{BX}/energyctrig_{STC}"] for BX in range(9)]
  else:
    hists = [f[f"BX{BX}/energytrig_{STC}"] for BX in range(9)]

  energy_edges = hists[0].axis(0).edges()
  energy_centers = (energy_edges[:-1] + energy_edges[1:])/2
  time_edges = hists[0].axis(1).edges()
  time_centers = (time_edges[:-1] + time_edges[1:])/2
  values = np.concatenate([hist.values()[:,93:125][:,::-1] for hist in hists], axis=1, dtype=float)

  energy_width = energy_edges[1:] - energy_edges[:-1]
  #values /= energy_width[:,np.newaxis] # normalise to energy bin width

  x = np.array([time_centers for i in energy_centers])
  y = np.array([energy_center*np.ones_like(time_centers) for energy_center in energy_centers])

  # max_values = np.argmax(values, axis=0)
  # max_energies = energy_centers[max_values]
  # pedestal = np.median(max_energies) + 1000
  # above_pedestal_values = values[energy_centers>pedestal,:]
  # vmax = above_pedestal_values.max()

  if decoded:
    value_suppression = 4
  else:
    value_suppression = 4
  h, xedges, yedges, im = ax.hist2d(x.flatten(), y.flatten(), bins=(time_edges, energy_edges), weights=values.flatten(), cmap="Reds", vmax=values.max()/value_suppression)
  #h, xedges, yedges, im = ax.hist2d(x.flatten(), y.flatten(), bins=(time_edges, energy_edges), weights=values.flatten(), cmap="Reds", vmax=vmax)

  #h, xedges, yedges, im = ax.hist2d(x.flatten(), y.flatten(), bins=(time_edges, energy_edges), weights=values.flatten(), cmap="Reds", norm=matplotlib.colors.LogNorm())

  if decoded:
    ax.set_ylim(0, 5000)

  ax.text(250, 14000, f"STC {STC}", verticalalignment="top", horizontalalignment="right", zorder=10)
  for i in range(1,9):
    ax.axvline(i*32, 0, 128, color="k", linestyle="--", linewidth=0.25)

  return im

def plotSingles(f, savepath, decoded=True):
  for STC in range(12):
    im = plot(f, STC, plt.gca(), decoded=decoded)
    plt.xlabel("Time between scintillator trigger and energy sample [a.u.]")
    plt.ylabel("STC Energy")

    cbar = plt.colorbar(im)
    cbar.set_label("Number of entries / bin height")

    plt.title(f"Relay {relayNumber}    Run {runNumber}")
    plt.savefig(savepath % {"STC":STC})
    plt.clf()

def plotAll(f, savepath, decoded=True):
  fig, axs = plt.subplots(3, 4, sharex=True, sharey=True)
  axs = axs.flatten()
  fig.subplots_adjust(hspace=0.05, wspace=0.05)
  for STC in range(12):
    ax = axs[STC]
    im = plot(f, STC, ax, decoded)

    if STC == 8:
      ax.set_xlabel("Time between scintillator trigger and energy sample [a.u.]", loc="left")
    elif STC == 0:
      ax.set_ylabel("STC Energy")
      ax.set_title(f"Relay {relayNumber}   Run {runNumber}\nN Entries {int(tools.getNEntries(f))}   Econt {econtNumber}", loc="left")
  
  cbar = fig.colorbar(im, ax=axs)
  cbar.set_label("Number of entries / bin height")

  fig.savefig(savepath, dpi=200)
  plt.close()

if __name__=="__main__":
  file_path = sys.argv[1]
  relayNumber = tools.getRelayNumber(file_path)
  runNumber = tools.getRunNumber(file_path)
  econtNumber = tools.getEcontNumber(file_path)
  outdir = tools.getPlotOutdir(file_path)
  os.makedirs(outdir, exist_ok=True)
  f = uproot.open(file_path)

  plotSingles(f, f"{outdir}/sample_vs_time_encoded_%(STC)d.png", decoded=False)
  plotAll(f, f"{outdir}/sample_vs_time_encoded_all.png", decoded=False)

  plotSingles(f, f"{outdir}/sample_vs_time_decoded_%(STC)d.png", decoded=True)
  plotAll(f, f"{outdir}/sample_vs_time_decoded_all.png", decoded=True)
  
  
