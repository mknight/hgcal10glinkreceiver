import uproot
import hexplot
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import tools

def getMean(f, hist_name, BX):
  h = f[f"BX{BX}/"+hist_name]

  values = np.array(h.values(), dtype=float)
  edges = np.array(h.axis().edges(), dtype=float)
  centers = edges[:-1]

  if values.sum() == 0:
    return 0
  else:
    return (values * centers).sum() / values.sum()
  
def getMeanLocation(f, STC_number, TCs, BX):
  h = f[f"BX{BX}/location_{STC_number}"]
  
  values = np.array(h.values(), dtype=float)
  values /= values.sum()

  x = sum([values[i]*TC.getCenter()[0] for i, TC in enumerate(TCs)])
  y = sum([values[i]*TC.getCenter()[1] for i, TC in enumerate(TCs)])
  return x, y

file_path = sys.argv[1]
relayNumber = tools.getRelayNumber(file_path)
runNumber = tools.getRunNumber(file_path)
econtNumber = tools.getEcontNumber(file_path)
outdir = tools.getPlotOutdir(file_path)
os.makedirs(outdir, exist_ok=True)
f = uproot.open(file_path)

pedastal_BX = 0

# choose between converted/decoded and "raw" values
#hist_names = [f"energy_{STC}" for STC in range(12)]
hist_names = [f"energyc_{STC}" for STC in range(12)]

all_means = [[getMean(f, hist_name, BX) for hist_name in hist_names] for BX in range(9)]
ped_means = np.array([getMean(f, hist_name, pedastal_BX) for hist_name in hist_names])
all_means = all_means - ped_means
all_means_maxes = [max(means) for means in all_means]
nominal_BX = np.argmax(all_means_maxes)
print(f"Chosen BX {nominal_BX} for Relay {relayNumber}")
means = all_means[nominal_BX]

base = 166.8/(2*np.sqrt(3))
STCs = hexplot.getCoarseDiamonds(base=base, offset_x=-2*base)

remapping = [4,6,5,9,2,8,10,3,11,1,7,0]
assert len(np.unique(remapping)) == 12
for i, STC in enumerate(STCs):
  STC.value = means[remapping[i]]
  STC.label = f"STC{remapping[i]}"

TCs = [STC.generateSet() for STC in STCs]

if econtNumber == "1":
  for STC in STCs:
    STC.rotate180()
  for TC_set in TCs:
    for TC in TC_set:
      TC.rotate180()

hexplot.plotDiamonds(STCs)

for i, TC_set in enumerate(TCs):
  x, y = getMeanLocation(f, i, TC_set, nominal_BX)
  if i == 0:
    plt.scatter([x], [y], color="k", marker="x", label="Average position of highest energy deposit")
  else:
    plt.scatter([x], [y], color="k", marker="x")
plt.ylim(top=plt.ylim()[1]+10)
plt.legend()

all_TCs = np.array(TCs).flatten()
hexplot.plotOutlines(all_TCs, linewidth=0.5)
hexplot.plotOutlines(STCs, linewidth=2)

for i, STC in enumerate(STCs):
  plt.text(STC.getCenter()[0], STC.getCenter()[1], STC.label, color="r", horizontalalignment="center", verticalalignment="center")
# for TC_set in TCs:
#   for i, TC in enumerate(TC_set):
#     plt.text(TC.x, TC.y, f"TC{i}", color="k", fontsize=8, horizontalalignment="left", verticalalignment="bottom")

stage_loc = {
  1691236311: [50, 167],
  1691244274: [50, 217],
  1691245113: [50, 117],
  1691245769: [-0.22, 165.92],
  1691246715: [100, 165.92],
  1691253960: [30, 180],
  1691257951: [30, 180],
  1691236311: [50, 167],
  1691434614: [-10, 249],
  1691435138: [30, 249],
  1691435891: [70, 249],
  1691436684: [90, 215],
  1691437325: [50, 215],
  1691437987: [10, 215],
  1691438598: [-30, 215],
  1691440946: [-50, 180],
  1691441561: [-10, 180],
  1691442252: [30, 180],
  1691443020: [70,180],
  1691443629: [110,180],
  1691444532: [90,145],
  1691445937: [50,145]
}
middle_reference = stage_loc[1691253960]

if int(relayNumber) in stage_loc:
  beam_center = (np.array(stage_loc[int(relayNumber)]) - np.array(middle_reference))
  beam_center[1] *= -1
  #beam_center += np.array([0,10])
  plt.scatter([beam_center[0]], [beam_center[1]], c="r", zorder=10)

plt.title(f"Relay {relayNumber}   Run {runNumber}   N Entries {int(tools.getNEntries(f))}\nChosen BX {nominal_BX}   Econt {econtNumber}")
plt.savefig(f"{outdir}/energy_hexaboard.png")
plt.savefig(f"{outdir}/energy_hexaboard.pdf")