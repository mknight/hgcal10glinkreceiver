import matplotlib.pyplot as plt
import sys
import uproot
import numpy as np

f = uproot.open(sys.argv[1])

#fig, axs = plt.subplots(3, 4, sharex=True, sharey=True)
fig, axs = plt.subplots(3, 4, sharex=True, sharey=False)
axs = axs.flatten()
#fig.subplots_adjust(hspace=0.05, wspace=0.05)

for STC in range(12):
  ax = axs[STC]

  all_centers = []
  all_values = []
  for BX in range(9):
    profile = f[f"BX{BX}/energytrigprof_{STC}"]
    edges = profile.axis().edges()
    centers = (edges[1:] + edges[:-1]) / 2
    values = profile.values()

    all_centers.append(centers[93:125] + 32*BX - 93)
    all_values.append(values[93:125][::-1])

  centers = np.concatenate(all_centers)
  values = np.concatenate(all_values)
  ax.plot(centers, values)

  print(centers)

  ax.text(0.9, 0.9, f"STC {STC}", verticalalignment="top", horizontalalignment="right", zorder=10, transform=ax.transAxes)
  for i in range(1,9):
    ax.axvline(i*32, 0, ax.get_ylim()[1], color="k", linestyle="--", linewidth=0.25)
  ax.set_xlim(0, 288)
plt.savefig(f"test.pdf")
plt.clf()