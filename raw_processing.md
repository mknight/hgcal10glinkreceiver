Some instructions to get started with processing the RAW ECONT data.

Clone the repository (mknight's fork)
```
git clone https://gitlab.cern.ch/mknight/hgcal10glinkreceiver.git
cd hgcal10glinkreceiver
```

Create a link to the test beam data
```
ln -s /eos/cms/store/group/dpg_hgcal/tb_hgcal/2023/BeamTestAug/ dat
```

The `econt_processor.cpp` script processes the raw data and outputs a bunch of 1D and 2D histograms containing information about super trigger cell energies, the location of the most energetic cell, and timing information from the scintillator trigger.

Compile the script
```
./compile.sh
```
and run the executable like
```
./econt_processor.exe $RELAY_NUMBER $RUN_NUMBER $ECONT_NUMBER $BX
```
with an example being
```
./econt_processor.exe 1691540024 1691540024 0 4
```

Whenever we triggered, we collected 9 BX worth of trigger data (+-4 either side of central BX) so BX 4 would be the central BX. Note, just because we intended BX 4 to be the central one, if we were not aligned properly, most of the signal may appear in later or earlier BX.

There are some python plotting scripts which will probably require a virtual environement to get working. If you have questions about them, let me know (matthew.knight@cern.ch or find me on Mattermost).
