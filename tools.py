def getRelayNumber(path):
  return path.split("Relay")[1].split("/")[0]

def getRunNumber(path):
  return path.split("Run")[1].split(".root")[0]

def getEcontNumber(path):
  return path.split("econt")[1].split("/")[0]

def getNEntries(f):
  return f[f"BX0/energy_0"].values().sum()

def getPlotOutdir(path):
  runNumber = getRunNumber(path)
  return "/".join(path.split("/")[:-1]).replace("output", "plots") + f"/Run{runNumber}"